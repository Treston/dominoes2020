﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public static class CountDominoes {

    public static string meshName = "default";

    [MenuItem("GameObject/Count Dominoes")]
    public static void Count () {
        MeshFilter[] filters = GameObject.FindObjectsOfType<MeshFilter>() as MeshFilter[];
        int count = 0;
        for (int i = 0; i < filters.Length; i++) {
            if (filters[i].sharedMesh.name == meshName) {
                count++;
            }
        }
        Debug.Log(count);
	}
}