﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimCotroller : MonoBehaviour
{
    public Animator animator;
    public string playName;

    void OnTriggerEnter(Collider c)
    {
        animator.Play(playName);
    }
}
